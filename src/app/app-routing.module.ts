import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const appRoutes: Routes = [
    {path: '', redirectTo: 'movie' , pathMatch: 'full'},
    {path: 'movie',  loadChildren: './movies/movies.module#MoviesModule'},
    {path: 'serie', loadChildren: './serie/serie.module#SerieModule'},
    { path: 'actor', loadChildren: './actor/actor.module#ActorModule'}
    //      {path: 'compose',
    // component: ComposeMessageComponent,
    // outlet: 'popup'
    // },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
