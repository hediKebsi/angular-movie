export interface ThemeInterface {
  name: string;
  url: string;
}
