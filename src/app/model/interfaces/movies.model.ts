export interface Movie {
  id: number;
  name: string;
  poster_path: string;
  release_date: string;
  vote_average: string;
  title: string;
}


