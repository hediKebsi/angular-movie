export * from './movies.component';
export * from './movie/movie.component';
export * from './upcoming/upcoming.component';
export * from './upcoming/upcoming.service';
export * from './genres/genres.component';
