import { APP_CONFIG } from './../core/core.config';
import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { Jsonp, URLSearchParams, RequestOptions, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Logger } from './logger.service';


@Injectable()
export class MoviesService {
  apikey: string;
  baseUrl: string;
  language: string;
  discover: string;
  movie: string;
  callbackJSON: string;
  constructor(private _jsonp: Jsonp, @Inject(LOCALE_ID) public locale: string, public _Logger: Logger,
   @Inject(APP_CONFIG) public config) {

    this.apikey =  this.config.APIKEY;
    this.baseUrl = this.config.apiEndpoint;
    this.language  = 'language=' + this.locale;
    this.discover = 'discover/';
    this.movie = 'movie?';
    this.callbackJSON = '&callback=JSONP_CALLBACK';

  }

  private SearchGet(search: URLSearchParams, url: string) {
//  const headers = new Headers({ 'Content-Type': 'application/json' });
//     const options = new RequestOptions({ headers: headers });
     search.set('api_key', this.apikey);
    search.set('language', this.locale);
    return this._jsonp.get(url, {search})
      .map(res => {
        this._Logger.log(url);
        return res.json();
      });
  }
  getPopular() {
    const search = new URLSearchParams();
    search.set('sort_by', 'popularity.desc');
    const url = this.baseUrl + this.discover + this.movie +  this.callbackJSON;
    return this.SearchGet(search, url);
  }

  getInTheaters() {
    const search = new URLSearchParams();
    search.set('primary_release_date.gt', '2015-10-20');
    search.set('primary_release_date.lte', '2015-12-20');
    search.set('sort_by', 'popularity.desc');
    const url = this.baseUrl + this.discover + this.movie  +  this.callbackJSON;
    return this.SearchGet(search, url);
  }

  getTopRatedMovies() {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'movie/top_rated?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  searchMovies(searchStr: string) {
    const search = new URLSearchParams();
    search.set('sort_by', 'popularity.desc');
    search.set('query', searchStr);
    const url = this.baseUrl +  'search/movie?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getMovie(id: string) {
    const search = new URLSearchParams();
    const url = this.baseUrl +  'movie/' + id + '?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getGenres() {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'genre/movie/list?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getMoviesByGenre(id: string) {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'genre/' + id + '/movies?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getMovieReviews(id: string) {
    const search = new URLSearchParams();
    const url = this.baseUrl  + 'movie/' + id + '/reviews?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }
  getMovieVideos(id: string) {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'movie/' + id + '/videos?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getSimilarMovies(id: string) {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'movie/' + id + '/similar?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getMovieCredits(id: string) {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'movie/' + id + '/credits?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getUpComingMovies() {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'movie/upcoming?callback=JSONP_CALLBACK';
    console.log(url);
    return this.SearchGet(search, url);
  }
  getPopularSeries() {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'tv/popular?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getTopRatedSeries() {
    const search = new URLSearchParams();

    return this._jsonp.get('https://api.themoviedb.org/3/tv/top_rated?language=fr-FR&callback=JSONP_CALLBACK', {search})
      .map(res => {
        return res.json();
      });
  }

  getSerieDetails(id: string) {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'tv/' + id + '?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }

  getSerieVideos(id: string) {
    const search = new URLSearchParams();
    const url = this.baseUrl + 'tv/' + id + '/videos?language=fr-FR&callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }


  inputSearch(query: string) {
    const search = new URLSearchParams();
    search.set('query', query);
    search.set('page', '1');
    const  url = this.baseUrl + 'search/multi?callback=JSONP_CALLBACK';
    return this.SearchGet(search, url);
  }
}
