import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '@ngxmovie/model';

@Component({
  selector: 'ngxmovie-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.css']
})
export class MovieCardComponent {

@Input('options') options: any;
@Output() Out: EventEmitter<any> = new EventEmitter();



  @Input()
  movie: Movie;
  constructor() {

  }
  onButtonClick(event) {
console.log(event);
}

  start() {

     this.Out.emit('i send it');
  }
}
