module.exports = {
    staticFileGlobs: [
        'dist/**/**.html',
        'dist/**/**.js',
        'dist/**/**.css',
        'dist/**/assets/**.png'
    ],
    root: 'dist',
    stripPrefix: 'dist/',
    navigateFallback: '/index.html',
};